import { Selector } from 'testcafe'; // first import testcafe selectors

fixture `Getting Started`// declare the fixture  
    .page `https://passport.tutorabc.com/customs/zh-tw/login`;  // specify the start page

//then create a test and place your code there
test('Test Login Success', async t => {  
    await t
        .click(Selector('#login-phone div').withText('電子郵件').nth(1))
        .typeText(Selector('#email'), 'MDQAFrankLiTR0425@test.com')
        .typeText(Selector('#login-email [class^="ant-input style_input-wrapper-input__78VCc input-f"]').nth(1), 'tutorabc123')
        .click('button')
        .expect(Selector('.memberDeskMenuBtn').innerText).eql('Hi! 土零四二五');
});

// test('Test Login Fail', async t => {  
//     await t
//         .typeText('#input_account', 'Frank')
//         .typeText('#input_password', 'wronf password')
//         .setNativeDialogHandler((type, text) => {
//             return true
            
//         })
//         .click('#button_submit');
    
//     const history = await t.getNativeDialogHistory();

//         await t
//         .expect(history[0].type).eql('alert')
//         .expect(history[0].text).eql('Please use correct account and password')
// });