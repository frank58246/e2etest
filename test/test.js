import { Selector } from 'testcafe'; // first import testcafe selectors

fixture `Getting Started`// declare the fixture  
    .page `https://frank58246.gitlab.io/e2etest/`;  // specify the start page

//then create a test and place your code there
test('Test Login Success', async t => {  
    await t
        .typeText('#input_account', 'Frank')
        .typeText('#input_password', '1234')
        .click('#button_submit')
        .expect(Selector('#msg').innerText).eql('Hello, Frank');
});

test('Test Login Fail', async t => {  
    await t
        .typeText('#input_account', 'Frank')
        .typeText('#input_password', 'wronf password')
        .setNativeDialogHandler((type, text) => {
            return true
            
        })
        .click('#button_submit');
    
    const history = await t.getNativeDialogHistory();

        await t
        .expect(history[0].type).eql('alert')
        .expect(history[0].text).eql('Please use correct account and password')
});